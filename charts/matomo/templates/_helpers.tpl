{{/* vim: set filetype=mustache: */}}

{{/*
Create a random alphanumeric password string.
We append a random number to the string to avoid password validation errors
*/}}
{{- define "matomo.randomPassword" -}}
{{- randAlphaNum 9 -}}{{- randNumeric 1 -}}
{{- end -}}

{{/*
Get the user defined password or use a random string
*/}}
{{- define "matomo.password" -}}
{{- $password := index .Values (printf "%sPassword" .Chart.Name) -}}
{{- default (include "matomo.randomPassword" .) $password -}}
{{- end -}}

{{/*
Get the user defined LoadBalancerIP for this release.
Note, returns 127.0.0.1 if using ClusterIP.
*/}}
{{- define "matomo.serviceIP" -}}
{{- if eq .Values.service.type "ClusterIP" -}}
127.0.0.1
{{- else -}}
{{- .Values.service.loadBalancerIP | default "" -}}
{{- end -}}
{{- end -}}

{{/*
Gets the host to be used for this application.
If not using ClusterIP, or if a host or LoadBalancerIP is not defined, the value will be empty.
When using Ingress, it will be set to the Ingress hostname.
*/}}
{{- define "matomo.host" -}}
{{- if .Values.ingress.enabled }}
{{- $host := .Values.ingress.hostname | default "" -}}
{{- default (include "matomo.serviceIP" .) $host -}}
{{- else -}}
{{- $host := index .Values (printf "%sHost" .Chart.Name) | default "" -}}
{{- default (include "matomo.serviceIP" .) $host -}}
{{- end -}}
{{- end -}}

{{/*
Return the proper Magento image name
*/}}
{{- define "matomo.image" -}}
{{- include "common.images.image" (dict "imageRoot" .Values.image "global" .Values.global) -}}
{{- end -}}

{{/*
Return the proper Docker Image Registry Secret Names
*/}}
{{- define "matomo.imagePullSecrets" -}}
{{- include "common.images.pullSecrets" (dict "images" (list .Values.image) "global" .Values.global) -}}
{{- end -}}

{{/*
Return the proper Storage Class
*/}}
{{- define "matomo.storageClass" -}}
{{- include "common.storage.class" (dict "persistence" .Values.persistence "global" .Values.global) -}}
{{- end -}}

{{/*
matomo credential secret name
*/}}
{{- define "matomo.secretName" -}}
{{- coalesce .Values.existingSecret (include "common.names.fullname" .) -}}
{{- end -}}

{{/*
Return the Database Type
*/}}
{{- define "matomo.databaseType" -}}
    {{- printf "%s" .Values.externalDatabase.type -}}
{{- end -}}

{{/*
Return the Database Hostname
*/}}
{{- define "matomo.databaseHost" -}}
    {{- printf "%s" .Values.externalDatabase.host -}}
{{- end -}}

{{/*
Return the Database Port
*/}}
{{- define "matomo.databasePort" -}}
    {{- printf "%d" (.Values.externalDatabase.port | int ) -}}
{{- end -}}

{{/*
Return the Database Name
*/}}
{{- define "matomo.databaseName" -}}
    {{- printf "%s" .Values.externalDatabase.database -}}
{{- end -}}

{{/*
Return the Database User
*/}}
{{- define "matomo.databaseUser" -}}
    {{- printf "%s" .Values.externalDatabase.user -}}
{{- end -}}

{{/*
Return the Database Secret Name
*/}}
{{- define "matomo.databaseSecretName" -}}
{{- if .Values.externalDatabase.existingSecret -}}
    {{- printf "%s" .Values.externalDatabase.existingSecret -}}
{{- else -}}
    {{- printf "%s-%s" (include "common.names.fullname" .) "externaldb" -}}
{{- end -}}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "matomo.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "common.names.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}