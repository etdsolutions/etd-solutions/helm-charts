{{/* vim: set filetype=mustache: */}}

{{/*
Create the name of the server service account to use
*/}}
{{- define "server.serverServiceAccountName" -}}
{{- if .Values.server.serviceAccount.create -}}
    {{ default (printf "%s-server" (include "common.names.fullname" .)) .Values.server.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.server.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Create the name of the worker service account to use
*/}}
{{- define "worker.workerServiceAccountName" -}}
{{- if .Values.worker.serviceAccount.create -}}
    {{ default (printf "%s-worker" (include "common.names.fullname" .)) .Values.worker.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.worker.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Create the name of the frontend service account to use
*/}}
{{- define "frontend.frontendServiceAccountName" -}}
{{- if .frontend.serviceAccount.create -}}
    {{ default (printf "%s-frontend-%s" (include "common.names.fullname" .context) .frontend.name) .frontend.serviceAccount.name }}
{{- else -}}
    {{ default "default" .frontend.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Create a random alphanumeric password string.
We append a random number to the string to avoid password validation errors
*/}}
{{- define "vendure.randomPassword" -}}
{{- randAlphaNum 9 -}}{{- randNumeric 1 -}}
{{- end -}}

{{/*
Get the user defined password or use a random string
*/}}
{{- define "vendure.password" -}}
{{- $password := .Values.vendure.password -}}
{{- default (include "vendure.randomPassword" .) $password -}}
{{- end -}}

{{/*
Get the user defined cookieSecret or use a random string
*/}}
{{- define "vendure.cookieSecret" -}}
{{- $password := .Values.vendure.cookieSecret -}}
{{- default (include "vendure.randomPassword" .) $password -}}
{{- end -}}

{{/*
Vendure credential secret name
*/}}
{{- define "vendure.secretName" -}}
{{- if .Values.vendure.existingSecret -}}
    {{- printf "%s" .Values.vendure.existingSecret -}}
{{- else -}}
    {{- printf "%s-%s" (include "common.names.fullname" .) "vendure" -}}
{{- end -}}
{{- end -}}

{{/*
Minio credential secret name
*/}}
{{- define "vendure.minioSecretName" -}}
{{- if .Values.minio.existingSecret -}}
    {{- printf "%s" .Values.minio.existingSecret -}}
{{- else -}}
    {{- printf "%s-%s" (include "common.names.fullname" .) "minio" -}}
{{- end -}}
{{- end -}}

{{/*
SMTP credential secret name
*/}}
{{- define "vendure.smtpSecretName" -}}
{{- if .Values.smtp.existingSecret -}}
    {{- printf "%s" .Values.smtp.existingSecret -}}
{{- else -}}
    {{- printf "%s-%s" (include "common.names.fullname" .) "smtp" -}}
{{- end -}}
{{- end -}}

{{/*
Return the DB Secret Name
*/}}
{{- define "vendure.databaseSecretName" -}}
{{- if .Values.externalDatabase.existingSecret -}}
    {{- printf "%s" .Values.externalDatabase.existingSecret -}}
{{- else -}}
    {{- printf "%s-%s" (include "common.names.fullname" .) "externaldb" -}}
{{- end -}}
{{- end -}}


{{/*
Elasticsearch credential secret name
*/}}
{{- define "vendure.elasticsearchSecretName" -}}
{{- if .Values.elasticsearch.existingSecret -}}
    {{- printf "%s" .Values.elasticsearch.existingSecret -}}
{{- else -}}
    {{- printf "%s-%s" (include "common.names.fullname" .) "elasticsearch" -}}
{{- end -}}
{{- end -}}
