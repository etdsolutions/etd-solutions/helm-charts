{{/* vim: set filetype=mustache: */}}

{{/*
Create the name of the backend service account to use
*/}}
{{- define "backend.backendServiceAccountName" -}}
{{- if .Values.backend.serviceAccount.create -}}
    {{ default (printf "%s-backend" (include "common.names.fullname" .)) .Values.backend.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.backend.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Create the name of the frontend service account to use
*/}}
{{- define "frontend.frontendServiceAccountName" -}}
{{- if .Values.frontend.serviceAccount.create -}}
    {{ default (printf "%s-frontend" (include "common.names.fullname" .)) .Values.frontend.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.frontend.serviceAccount.name }}
{{- end -}}
{{- end -}}
