
#!/usr/bin/env bash

cat <<EOF
include: '/.gitlab-ci.tmpl.yml'

stages:
  - test
  - install
  - release

EOF

for f in $(find charts/* -maxdepth 0 -type d)
do
  CHART_VERSION=$(grep "^version:" ${f}/Chart.yaml | cut -d' ' -f2-)
  cat <<EOF
'${f##*/}:lint':
  stage: test
  extends: .lint
  variables:
    HELM_NAME: "${f##*/}"
  rules:
    - if: '\$CI_COMMIT_BRANCH == "master"'
      changes:
        - ${f}/**/*

'${f##*/}:dev_push':
  stage: release
  extends: .dev_push
  variables:
    HELM_NAME: "${f##*/}"
  rules:
    - if: '\$CI_COMMIT_BRANCH == "master"'
      changes:
        - ${f}/**/*

'${f##*/}:release':
  stage: release
  extends: .release
  variables:
    HELM_NAME: "${f##*/}"
    CHART_VERSION: "${CHART_VERSION}"
  rules:
    - if: '\$CI_COMMIT_BRANCH == "master"'
      changes:
        - ${f}/Chart.yaml

EOF

done
