FROM gitpod/workspace-base:2022-06-09-20-58-43

ENV HELM_VERSION=3.9.0 \
    KUBECTL_VERSION=1.24.0

USER root

RUN cd /tmp && \
    wget -c https://get.helm.sh/helm-v$HELM_VERSION-linux-amd64.tar.gz  -O - | tar -xz && \
    mv linux-amd64/helm /usr/local/bin && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/v$KUBECTL_VERSION/bin/linux/amd64/kubectl && \
    mv kubectl /usr/local/bin && \
    chmod +x /usr/local/bin/kubectl && \
    rm -rf /tmp/linux-amd64

USER gitpod
